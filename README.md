# Cumbria Unitary Authorities LocalGov Drupal parent theme

A sub-theme of the [LocalGov Drupal base theme](https://github.com/localgovdrupal/localgov_base), for Cumbrian unitary authorities, Cumberland, and Westmorland & Furness.

This theme is designed to be used as a parent theme.

See the [LocalGov Drupal documentation](https://localgovdrupal.org/) for theme requirements, setup, and usage details.
