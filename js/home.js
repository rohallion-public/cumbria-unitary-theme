(function homeServicesToggle(Drupal) {
    Drupal.behaviors.homeServicesToggle = {
        attach: function (context) {
            // Only the first two service IA rows show by default.
            const moreServices = [...document.querySelectorAll(".layout--threecol-33-34-33")].slice(2);

            if (moreServices) {

                const lastServices = moreServices.slice(-1);
                lastServices[0].style.marginBottom = '3rem';

                moreServices.forEach(element => {
                    element.style.display = "none";
                });

                // Add show/hide buttons to the bottom of the IA.
                let moreServicesButton = document.createElement("button");
                moreServicesButton.classList.add("btn-services");
                moreServicesButton.innerHTML = "Show all services";
                let moreServicesButtonContainer = document.createElement("div");
                moreServicesButtonContainer.appendChild(moreServicesButton);
                document.querySelectorAll(".layout--threecol-33-34-33")[1].appendChild(moreServicesButtonContainer);

                moreServicesButton.addEventListener("click", () => {
                    moreServices.forEach(element => {
                        element.style.display = "flex";
                    });
                    moreServicesButtonContainer.style.display = "none";
                });
            }
        }
    };
}(Drupal));
