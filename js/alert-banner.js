(function alertBannersScript(Drupal) {
    Drupal.behaviors.banners = {
        attach(context) {
            context = context || document;
            const alertBanners = context.querySelectorAll('.localgov-alert-banner:not([style*="display: none"])');
            if (alertBanners.length) {
              const closeDiv = document.createElement("div");
              closeDiv.className = 'alert-banner__close';
              const closeButton = document.createElement("button");
              closeButton.setAttribute('aria-label', 'Close all notifications');
              const closeIcon = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
              closeIcon.setAttribute('xmlns', 'http://www.w3.org/2000/svg');
              closeIcon.setAttribute('viewBox', '0 0 320 512');
              const closeIconPath = document.createElementNS('http://www.w3.org/2000/svg', 'path');
              closeIconPath.setAttribute('fill', '#ffffff');
              closeIconPath.setAttribute('d', 'M207.6 256l107.72-107.72c6.23-6.23 6.23-16.34 0-22.58l-25.03-25.03c-6.23-6.23-16.34-6.23-22.58 0L160 208.4 52.28 100.68c-6.23-6.23-16.34-6.23-22.58 0L4.68 125.7c-6.23 6.23-6.23 16.34 0 22.58L112.4 256 4.68 363.72c-6.23 6.23-6.23 16.34 0 22.58l25.03 25.03c6.23 6.23 16.34 6.23 22.58 0L160 303.6l107.72 107.72c6.23 6.23 16.34 6.23 22.58 0l25.03-25.03c6.23-6.23 6.23-16.34 0-22.58L207.6 256z');
              closeIcon.appendChild(closeIconPath);
              closeButton.appendChild(closeIcon);
              const closeText = document.createTextNode("Close all notifications");
              closeButton.appendChild(closeText);
              closeDiv.appendChild(closeButton);
              const targetDiv = alertBanners[alertBanners.length - 1].querySelector('.localgov-alert-banner__inner');
              targetDiv.appendChild(closeDiv);

              document.addEventListener('click', e => {
                if (e.target.closest('.alert-banner__close')) {
                  function getCookie(name) {
                    let value = '; ' + document.cookie;
                    let parts = value.split(`; ${name}=`);
                    if (parts.length === 2) return parts.pop().split(';').shift();
                  }

                  e.preventDefault();

                  let alertBannerCookie = getCookie('hide-alert-banner-token');
                  let cookieTokens = typeof alertBannerCookie !== 'undefined' ? alertBannerCookie.split('+') : [];
                  alertBanners.forEach((alertBanner) => {
                    if (!cookieTokens.includes(alertBanner.dataset.dismissAlertToken)) {
                      cookieTokens.push(alertBanner.dataset.dismissAlertToken);
                      alertBanner.style.display = 'none';
                    }
                  });
                  const newCookieValue = cookieTokens.join('+');
                  document.cookie = 'hide-alert-banner-token=' + newCookieValue + '; path=/; max-age=2592000;';
                }
              });
            }
        }
    };
}(Drupal));
