/**
 * @file JS file for disclosure controls.
 */
(function cookieScript(Drupal) {
  Drupal.behaviors.cookie = {
    attach(context) {
      context = context || document;

      const getCookieValue = function (name) {
        return document.cookie.match("(^|;)\\s*" + name + "\\s*=\\s*([^;]+)") ? document.cookie.match("(^|;)\\s*" + name + "\\s*=\\s*([^;]+)").pop() : ""
      };
      const cookieBanner = document.querySelector('.region-pre-header');

      if (
        cookieBanner &&
        !getCookieValue("CookieControl")
      )
      {
        cookieBanner.style.display = 'block';
      }



    }
  };
}(Drupal));
